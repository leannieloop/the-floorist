package com.sg.floorist.model;

import java.math.BigDecimal;

public class Tax {
    private String state;
    private BigDecimal taxRate;

    public Tax(String state, String taxRateString) {
        this.state = state;
        this.taxRate = new BigDecimal(taxRateString);
    }

    public String getState() {
        return state;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }
}