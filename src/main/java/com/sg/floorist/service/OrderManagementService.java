package com.sg.floorist.service;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;
import com.sg.floorist.model.OrderValidationResponse;
import com.sg.floorist.model.Product;
import com.sg.floorist.model.Tax;

import java.time.LocalDate;
import java.util.List;

public interface OrderManagementService {
    List<Order> getOrdersForGivenDate(LocalDate date) throws DataPersistenceException;
    List<Tax> getAllStateTaxRates() throws DataPersistenceException;
    List<Product> getAllProducts() throws DataPersistenceException;
    OrderValidationResponse create(Order newOrder);
    Order add(Order newOrder) throws DataPersistenceException;
    OrderValidationResponse getOrderByOrderNumber(Integer orderNumber);
    OrderValidationResponse update(Order updatedOrder);
    Order remove(Order order);
    void saveChanges() throws DataPersistenceException;
}
