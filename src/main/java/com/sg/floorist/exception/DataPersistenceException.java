package com.sg.floorist.exception;

public class DataPersistenceException extends Exception {

    public DataPersistenceException(String message) {
        super(message);
    }

    public DataPersistenceException(String message, Throwable e) {
        super(message, e);
    }
}