package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Product;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductDaoImpl implements ProductDao {
    private static final String PRODUCTS_FILE = "Products.txt";
    private static final String DELIMITER = ",";

    private List<Product> products = new ArrayList<>();

    @Override
    public List<Product> getAllProducts() throws DataPersistenceException {
        loadProductsToList();
        return products;
    }

    private void loadProductsToList() throws DataPersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(PRODUCTS_FILE)));
        } catch (FileNotFoundException e) {
            throw new DataPersistenceException("Could not load product information.", e);
        }
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            String[] productAttributes = currentLine.split(DELIMITER);
            Product product = new Product(productAttributes[0], productAttributes[1], productAttributes[2]);
            products.add(product);
        }
        scanner.close();
    }
}
