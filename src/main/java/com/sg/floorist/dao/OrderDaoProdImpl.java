package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Order;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class OrderDaoProdImpl implements OrderDao {

    private static final String DELIMITER = ",";

    private Map<LocalDate, List<Order>> ordersByDate = new HashMap<>();
    private Map<LocalDate, List<Order>> currentSessionChanges = new HashMap<>();

    @Override
    public Map<LocalDate, List<Order>> getOrders(LocalDate date) throws DataPersistenceException {
        loadOrdersToMap(date);
        return ordersByDate;
    }

    @Override
    public Order add(Order newOrder) {
        LocalDate today = LocalDate.now();
        try {
            getOrders(today);
        } catch (DataPersistenceException e) {
            List<Order> orders = new ArrayList<>();
            ordersByDate.put(today, orders);
        }
        ordersByDate.get(today).add(newOrder);
        currentSessionChanges.put(today, ordersByDate.get(today));
        return newOrder;
    }

    @Override
    public Order update(Order updatedOrder) {
        for (LocalDate date : ordersByDate.keySet()) {
            if (ordersByDate.get(date).contains(updatedOrder)) {
                currentSessionChanges.put(date, ordersByDate.get(date));
            }
        }
        return updatedOrder;
    }

    @Override
    public Order remove(Order order) {
        for (LocalDate date : ordersByDate.keySet()) {
            if (ordersByDate.get(date).contains(order)) {
                ordersByDate.get(date).remove(order);
                currentSessionChanges.put(date, ordersByDate.get(date));
            }
        }
        return order;
    }

    @Override
    public void saveChanges() throws DataPersistenceException {
        writeOrdersToFile(currentSessionChanges);
    }

    private void loadOrdersToMap(LocalDate date) throws DataPersistenceException {
        String stringDate = date.format(DateTimeFormatter.ofPattern("MMddyyyy"));
        String orderFile = "orders/Orders_" + stringDate + ".txt";
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(orderFile)));
        } catch (FileNotFoundException e) {
            throw new DataPersistenceException(
                    "Could not load order information. Orders may not exist for the given date.", e);
        }
        List<Order> orders = new ArrayList<>();
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            String[] orderAttributes = currentLine.split(DELIMITER);
            orderAttributes = readOrderAttributesForDelimiterInCustomerName(orderAttributes);
            Order order = new Order(orderAttributes[0], orderAttributes[1], orderAttributes[2],
                    orderAttributes[3], orderAttributes[4], orderAttributes[5],
                    orderAttributes[6], orderAttributes[7], orderAttributes[8],
                    orderAttributes[9], orderAttributes[10], orderAttributes[11]);
            orders.add(order);
        }
        ordersByDate.put(date, orders);
        scanner.close();
    }

    private String[] readOrderAttributesForDelimiterInCustomerName(String[] orderAttributes) {
        if (orderAttributes.length == 13) {
            String[] orderAttributesDelimiterInCustomerName = new String[orderAttributes.length - 1];
            orderAttributesDelimiterInCustomerName[0] = orderAttributes[0];
            orderAttributesDelimiterInCustomerName[1] = orderAttributes[1].substring(1) + ","
                    + orderAttributes[2].substring(0, orderAttributes[2].length() - 1);
            System.arraycopy(orderAttributes, 3, orderAttributesDelimiterInCustomerName, 2, 10);
            return orderAttributesDelimiterInCustomerName;
        } else {
            return orderAttributes;
        }
    }

    private void writeOrdersToFile(Map<LocalDate, List<Order>> currentSessionChanges) throws DataPersistenceException {
        for (LocalDate date : currentSessionChanges.keySet()) {
            String stringDate = date.format(DateTimeFormatter.ofPattern("MMddyyyy"));
            PrintWriter out;
            try {
                out = new PrintWriter(new FileWriter("orders/Orders_" + stringDate + ".txt"));
            } catch (IOException e) {
                throw new DataPersistenceException("Could not save order information.", e);
            }
            out.println("OrderNumber" + DELIMITER
                    + "CustomerName" + DELIMITER
                    + "State" + DELIMITER
                    + "TaxRate" + DELIMITER
                    + "ProductType" + DELIMITER
                    + "Area" + DELIMITER
                    + "MaterialCostPerSquareFoot" + DELIMITER
                    + "LaborCostPerSquareFoot" + DELIMITER
                    + "MaterialCost" + DELIMITER
                    + "LaborCost" + DELIMITER
                    + "Tax" + DELIMITER
                    + "Total");
            currentSessionChanges.get(date)
                    .stream()
                    .map(o -> {
                        out.println(o.getOrderNumber() + DELIMITER
                                + (o.getCustomerName().contains(",")
                                ? "\"" + o.getCustomerName() + "\""
                                : o.getCustomerName())
                                + DELIMITER
                                + o.getState() + DELIMITER
                                + o.getTaxRate() + DELIMITER
                                + o.getProductType() + DELIMITER
                                + o.getArea() + DELIMITER
                                + o.getMaterialCostPerSquareFoot() + DELIMITER
                                + o.getLaborCostPerSquareFoot() + DELIMITER
                                + o.getMaterialCost() + DELIMITER
                                + o.getLaborCost() + DELIMITER
                                + o.getTax() + DELIMITER
                                + o.getTotal());
                        return o;
                    }).forEachOrdered(o -> out.flush());
        }
    }
}
