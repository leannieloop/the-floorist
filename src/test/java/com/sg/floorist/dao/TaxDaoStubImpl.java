package com.sg.floorist.dao;

import com.sg.floorist.exception.DataPersistenceException;
import com.sg.floorist.model.Tax;

import java.util.ArrayList;
import java.util.List;

public class TaxDaoStubImpl implements TaxDao {
    @Override
    public List<Tax> getAllStateTaxRates() throws DataPersistenceException {
        List<Tax> testTaxes = new ArrayList<>();
        testTaxes.add(new Tax("AK", "0.00"));
        testTaxes.add(new Tax("CA", "7.25"));
        return testTaxes;
    }
}
